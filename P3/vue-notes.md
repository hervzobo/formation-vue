# Les Styles

Les **preprocesseurs** permettent d'étendre les fonctionnalités CSS comme les opérateurs, les fonctions, les mixins....
Format de fichiers **préprocesseurs**:
- Sass => **.sass* ou **.scss*
- LESS => **.less*.

Utilisation des préprocesseurs:
- Effectuer une configuration initiale du *préprocesseur* choisi;
- Utiliser la prop *lang* au bloc de style:
```css
<style lang="scss">
  .button{
    &.is-small{...}
    &.is-large{...}
  }
</style>
```
## Styles délimités (scoped styles)
- permet de fournir un attribut ```scoped``` au bloc style afin que le css ne s'applique qu'aux éléments du composant courant.
- inconvénient du **scoped sytles** peut être affectée par des styles présentant une grande spécifité.

## Création de modules
- appréciés pour les possibilités de modularité et de composition de css offerte. 

Exemple de création d'un module:
```css
<style module>
  .red{
    color: red;
  }
  .bold {
    font-weight: bold;
  }
</style>
```

Exemple d'utilisation:
```html
<template>
  <h1 :class="$style.red">Hello world!</h1>
</template>
```

# Passer les data entre composants
Utilisation de props en tant qu'objet.

```javaScript
<script>
  export default {
    props: {
      items: {type: Array},
      doShopping: {type: Function},
      title: {
        type: String,
        required: true //rendre le champ obligatoire
      },
      count: {
        type: Number,
        default: 1
      }
    }
  }
</script>
```
# Les événements

pour créer un événement, utiliser la fonction ```$emit```.

Les paramètres de la fonction ```$emit```:
- le nom de l'événement définit en *kebab-case*.
- un payload optionnel utiliser pour passer des données à l'écouteur de l'événement.

```javaScript
<script>
export default {
  methods: {
    emitEvent() {
      this.$emit('custom-event-name', {message: 'Hello world!'});
    }
  }
}
</script>
```
## Ecoutez et répondez à un événement émis
- l'écoute d'un événement se fait avec ```v-on``` ou ```@```.
- Pour écouter un événement:
  - Ajouter un écouteur d'événement (*listener*) ```@``` avec le nom de l'événement personnaliser sur le composant émettant l'événement.
  - Lui assigner une méthode recevant le payload comme premier argument s'il existe.
```javaScript
<template>
    <div>
        <p>{{ message }}</p>
        <!-- Écoute les événements personnalisés et y assigne une méthode -->
        <ChildComponent @custom-event-name="setMessage" />
    </div>
</template>

<script>
import ChildComponent from './ChildComponent'

export default {
    components: { ChildComponent },
    data () {
        return {
            message: 'Hello'
        }
    },
    methods: {
        // Définit la méthode utilisée par le payload pour mettre à jour la propriété data
        setMessage(payload) {
            this.message = payload.message
        }
    }
}
</script>
````
**Note**: cette technique ne fonctionne que, lorsqu'on communique directement depuis un composant enfant vers un composant parent.

# Composants fléxibles avec les slots

- ```slot``` sert d'emplacement pour du contenu que le développeur peut définir dynamiquement sans utilisé les ```props```.
- Très utile lorsque nous disposons de plusieurs propriétés.