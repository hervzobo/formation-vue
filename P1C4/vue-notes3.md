

## Les directives vue

- Fournissent un moyen standard de résolution de problèmes
- Elles sont identifiables par le préfixe ``` v- ```

Les directives les plus courantes de vue:
- ```v-if```, ```v-else-if```, ```v-else```: *conditionner l'affichage de contenu.*
- ```v-show```: *contrôler la visibilité d'un élément faisant objet d'une permutation (toggle). Exemple: d'affichage d'une modal*
**Note**: *```v-show``` permute la visibilité de l'élément HTML grâce au CSS alorsque ```v-if``` supprime complètement l'élément du DOM.*
- ```v-for```: *Boucler sur un contenu. Suite la syntaxe de boucle standard de javaScript ```for ... in```*
- ```v-bind```: *définir les attributs HTML dynamiquement*
exemple:

```javaScript
<div id="app">
    <ul>
        <li v-for="item in apiResponse">
            <a v-bind:href="item.url">{{ item.name }}</a>
        </li>
    </ul>
</div>

<script>
    const app = new Vue({
        el: '#app',
        data: {
            apiResponse: [
                { name: 'GitHub', url: '<https://www.github.com>' },
                { name: 'Twitter', url: '<https://www.twitter.com>' },
                { name: 'Netlify', url: '<https://www.netlify.com>' }
            ]
        }
    })
</script>
```
**Note**: ```v-bind``` couramment utilisé via son abréviation (:). ``` :href="item.url" ```
- ```v-on```: *Ecoute et réponse aux événements*
**Note**: abréviation ```@```.  A la place de ``` v-on:click()```, on pourrait écrire ``` @click()```
- ```v-model```

### Les méthodes avec ```methods```

permettent de définir des fonctions auxquelles l'application vue aura accès.
```javaScript
const app = new Vue({
 el: '#app',
 methods: {
  //définition de la méthode comme en javaScript standard
  sayHello(){ return "Hello world!"}
 }
})
```
### Formulaires

Pour utiliser les formulaires, on utiliser *la directive ``` v-model```* (**two binding**) 



