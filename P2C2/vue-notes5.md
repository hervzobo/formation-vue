# Les composants
- permettent d'encapsuler un ensemble d'éléments HTML, réutilisable et facilement maintenable.
- Fournissent une base pour les applications scalbles et maintenables

## Les composants monofichiers
- Permettent de créer des éléments HTML personnalisés et facilement maintenables.
- Constituent une caractéristique crucial de vue et identifiables par leur extension *.vue*.
- Composés de 3 blocs:

  - Script: du code javaScript
  - Template: du code HTML
  - Style: du code CSS
```javaScript
<script>
  export default {
    name: 'Home'
  }
</script>
<template>
  <a href='/'>Home</a>
</template>
<style>
  a{text-decoration: none;}
</style>
```
## Importer un composant dans un autre

- on utilise la propriété **components**

```javaScript
<script>
import Home from './components/Home.vue'
  export default {
    name: 'About',
    components: {Home}
  }
</script>
<template>
  <Home/>
</template>
```

## Communication entre composants
- La communication est possible grâce à la propriété ``` Props```

