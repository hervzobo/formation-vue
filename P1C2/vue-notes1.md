

## le monde des app monopages

 **inconvénients du monde traditionnel du déveoppement web**
* chargement d'une nouvelle page web à chaque demande
* détérioration de l'expérience utilisateur
**Concept des SPA (Single Page Application)**
* chargement d'une seule fois de la page web
* la page est mise à jour distinctement sans recharger la page entière.

## Vue framework
framework similaires à Vue: React, Angular, polymer...

### Avantages de Vue 

* simple à apprendre juste quelques notions en HTML, CSS et JavaScript suffisent
* accessibilité à dévenir contributeur
* Augmentation de la productivité des développeurs.
---

### Intégrer Vue à son projet via CDN (*content delivery network*)

```javaScript
<!DOCTYPE html>
<html lang="en">
 <body>
  <div class="app">	
  </div>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
 </body>
</html>
```
---

### Créer une nouvelle instance de Vue

```javaScript
<!DOCTYPE html>
<html lang="en">
	
 <body>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script>
   const app = new Vue({
   })
  </script>
 </body>
</html>
```
---

### Indiquer à quel niveau intégrer Vue dans votre page via un selecteur CSS

```javaScript
<!DOCTYPE html>
<html lang="en">
	
 <body>
  <div class="app">	
  </div>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script>
   const app = new Vue({
    el: '.app'	
   })
  </script>
 </body>
</html>
```
**Note**: *utiliser un selecteur ID est recommandé.*
---

### Stocker les données

ajouter la propriété **data**. sa valeur est un objet et accepte n'importe quelle *paire clé-valeur.*
```javaScript
<script>
const app = new Vue({
	el: '.app',
	data:{
	restaurantName: 'Enable'
	}
})
</script>
```
---

### Afficher le rendu des données

```javaScript
<div class="app">
	<h1>{{ restaurantName }}</h1>	
</div>
```
---




