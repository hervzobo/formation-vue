import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    shoppingCart: 0,
    restaurantName: "La belle vue",
    simpleMenu: [
      {
        name: "Croissant",
        image: {
          source: "/images/crossiant.jpg",
          alt: "Un croissant"
        },
        inStock: true,
        quantity: 1,
        price: 1800
      },
      {
        name: "Baguette de pain",
        image: {
          source: "/images/french-baguette.jpeg",
          alt: "Quatre baguettes de pain"
        },
        inStock: true,
        quantity: 1,
        price: 2400
      },
      {
        name: "Éclair",
        image: {
          source: "/images/eclair.jpg",
          alt: "Éclair au chocolat"
        },
        inStock: false,
        quantity: 1,
        price: 3000
      }
    ]
  
  },
  mutations: {
    copyright: state => {
			const currentYear = new Date().getFullYear()

			return `Copyright ${state.restaurantName} ${currentYear}`
    },
    UPDATE_SHOPPING_CART: (state, amount) => {
      state.shoppingCart += Number(amount);
    }
  },
  actions: {
    updateShoppingCart(context,amount) {
      context.commit('UPDATE_SHOPPING_CART',amount);
    }
  },
  modules: {
  }
})
