# Le state managment 
- **Ojectif du state managment**: *s'assurer que l'application utilise les bonnes données à tout moment.*
- Concept d'une **source unique de vérité (SSoT: Single Source of Truth)** est *d'éviter la duplication inutile de données* dans l'application.

## Data store centralisé avec vuex
- *vuex* est un gestionnaire d'état qui permet de créer un *data store centralisé*.
- commande d'installation:
```
vue add vuex
```
- *store/index.js structure:*
```javaScript
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {

    },
    mutations: {

    },
    actions: {

    }
})
```

``` Vue.use(Vuex) ``` permet d'ajout globalement la fonctionnalité ``` Vuex ``` à notre instance ``` Vue ```.

## Récupération de données depuis Vuex

- Le *state* est comparable à la propriété *data* dans un composant.
- Ajout de données dans le store:
```javaScript
export default new Vuex.store({
  state: {
    name: 'yvon',
    today: 31
  }
})
```
- l'idée du *store* n'est pas de tout mettre à l'intérieur mais d'y mettre les données qui seront utilisées globalement dans l'application.
- Il existe deux façons de récupérer les données du store:
  - Utilisation ``` $store ```
  
  En considérant l'exemple plus haut, on accède à notre ``` $store ```:
  ```javaScript
  <template>
    <p> {{ $store.state.name }} </p>
  </template>
  ```
  - Utilisation du ``` mapState ```: permet de demander à ``` Vuex ``` les propriétés du ```state``` de notre choix et les ajoute à nos propriétés calculées (*computed properties*):

  ```javaScript
  <template>
      <p> {{ name }} </p>
  </template>

  <script>
  import { mapState } from 'vuex';

  export default {
      computed: {
          ...mapState(['name', 'today'])
      }
  }
  </script>
  ```
*...* syntaxe de *décomposition* avant ```mapState``` (pas obigatoire)

### les getters
- Les *getters* sont équivalent à la propriété calculée (*computed*):

```javaScript
export default new Vuex.Store({
    state: {
        month: 07,
        day: 23,
        year: 2010
    },
    getters: {
        formattedDate: state => {
            return `${state.day}-${state.month}-${state.year}`
        }
    }
}
```
- Chaque *getter* est une fonction qui prend en paramètre le *state* et retourne une valeur.
- Utilisation de ```mapGetters```:
```javaScript
<template>
    <p>La date stockée dans Vuex est le {{ formattedDate }}.</p>
</template>

<script>
import { mapGetters } from 'vuex';

export default {
    computed: {
        ...mapGetters(['formattedDate'])
    }
}
</script>
```

### Les mutations
- responsable de la modification des données dans le ```state```.
- rendent le code plus facile à *debugger*.
- La convention de nommage dans les mutations consiste à utiliser utiquement des majuscules avec des underscores pour la séparation des mots. Cela permet d'identifier une fonction normale avec une *mutation*.
```javaScript
export default new Vuex.store({
  state: {count:0},
  mutations: {
    INCREASE_COUNT(state) {
      state.count +=1;
    },
  actions: {}
  }
})
```
- la *mutation* reçoit par defaut comme premier argument le *state*
- Ajout d'un payload:
```javaScript
mutations: {
  INCREASE_COUNT(state, increment) {
    state.count += new Number(increment);
  }
}
```
- **Acter une mutation** consiste à utiliser ``` commit ``` lors de l'appel de la *mutation*. L'action ``` commit ``` prend deux paramètres:
  - nom le de la mutation
  - payload (facultatif)
  ``` this.$store.commit('INCREASE_COUNT', 2) ```
  **Note**: Faire appel d'une *mutation* dans un composant vue **n'est pas une bonne pratique**.
- Les *mutations vuex* sont **synchrones**, cela veut dire qu'on ne peut pas par exemple récupérer des données d'une API dans une mutation.

### Les Actions
- correspond à la propriété ```methods``` dans une instance Vue.
```javaScript
export default new Vuex.store({
  state: {count:0},
  mutions: {
    INCREASE_COUNT(state, amount){
      state.count += Number(amount);

    }
  },
  actions: {
    incrementCount(context, amount){
      context.commit('INCREASE_COUNT',amount)
    }
  }
})
```
- une *action* est composée:
  - d'un nom;
  - du paramètre ``` context ```;
  - et du ```payload``` (facultatif)
- l'**affectaion par décomposition** (en anglais: *desctructuring*) est une technique de codage qui permet de simplifier la lecture du code.
- Les *actions* sont **asynchrones**.

#### Utiliser les Actions dans les composants
- propager( *dispatch*) ou *mapActions*

Exemple d'utilisation de *dispatch*
```javaScript
methods: {
  sendIncrement(){
    this.$store.dispatch('incrementCount');
  }
}
```
Exemple d'utilisation de *mapActions*
```javaScript
<script>
import {mapState, mapActions} from 'vuex';
export default {
  computed: {
    ...mapState(['count'])
  },
  methods: {
    ...mapActions(['incrementCount'])
  }
}
</script>
```