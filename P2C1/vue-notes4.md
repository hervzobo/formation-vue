
# Vue CLI

- Les outils de build (*gulp, wepback...*) permettent d'automatiser un certains nombres de process
- Avantage plus de temps dans la construction qu'à la configuration de l'app.

## Installation de Vue CLI
 
- Requis Node.JS v9+
```
npm install -g @vue/cli
//ou
yarn global add @vue/cli

```
- S'assurer que *vue* s'est bien installé, 
en console taper
```
vue --version
```
output
```
@vue/cli 4.3.1
```
### Créer un nouveau projet
```
vue create my-vue-project
```
- choisir un **preset** ou encore les options de configurations
- Pour gérer vous projets vue, utiliser vue ui.
En ligne de commande taper
```
vue ui
```
## Architecture d'une application Vue

* *node_modules*: ensemble des dépendances gérer par **npm**
* *public*: continet favicon.ico et ```index.html``` de base
* *src*: contient la quasi-totalité du code
* *.gitIgnore*: contient une liste de fichiers ou dossier non attachés au repository. exple /dist, node_modules...
* *package.json*: fichier de configuration de base du projet
* *src -> assets*:ou placer les images, et autres ressources de l'app
* *src -> components*: contient les composants de l'application
* *src -> main.js*: défini les options de configurations de vue

### run l'application

```
npm run serve
```

