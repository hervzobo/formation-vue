
# Cycle des composants

- Cycle de vie (life cycle) étapes par lesquelles un composant passe, de sa création jsuqu'à sa disparition.

## Les 3 étapes du cycle de vie d'un composant

- **Create**: phase de *construction* d'un composant
- **Mount**: phase de *rendu* de la vue
- **Destroy**: phase de *retrait* du composant de la page.

  ## Les hooks de cycle de vie d'un composant (life cycle hooks)
- viennnent résoudre le problème de timing
- permettent d'accéder à des événements spécifiques autour de ces étapes
- Les principaux hooks:

  - **beforeCreate**:
  - **created**:
  - **beforeMount**:
  - **mounted**:
  - **beforeDestroy**:
  - **destroyed**:
  **Note**: *Chaque étape est accompagnée de 2 hooks.*
  [Schéma du cycle de vie d'un composant](https://fr.vuejs.org/images/lifecycle.png)
## Utilisation des hooks
De la même manière que *data*, il faut juste définir la propriété:

```javaScript
<script>
  export default {
    data() {},
    beforeCreate() {
      console.log('Hello World!')
    }
  }
</script>
```