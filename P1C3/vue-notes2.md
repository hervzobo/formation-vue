
## Stockage et gestion de données

### Données réactives

#### Propriétés calculées (*computed properties*)

- définir une valeur réutilisable, se mettant à jour en fonction d'autres propriétés *data*.
- **Chaque propriété calculée est une fonction retournant une valeur.**

exemple:

```javaScript
<script>
const app = new Vue({
 el: "#app",
 data: {
  restaurantName: "Enable Café avec vue",
  email: 'contact@enable.cm',
  telephone: '+237 672 492 144',
  adresse: '1406 route du gof, Yaoundé, Cameroun'
 },
 computed:{
  enFooter(){
   return this.restaurantName+" "+ new Date().getFullYear();
  }
 }
})
</script>
```
---
---
