
# Vue router

- bibliothèque: **vue-router**
- Ajout **vue-router** dans une app:
```
vue add router
```

## Anatomie du routeur

- chaque route possède 3 propriétés:
  - **path**: URL correspondant au composant;
  - **name**: le nom de la route utilisé plus pour étiqueter et débugguer l'application;
  - **component** : le composant a affiché lorsque le *path* est trouvé.

## Fonctionnement de Vue Router
Deux principaux composants à connaître:
- ``` <router-view></router-view> ```, définit la zone de la page dans laquelle apparaîtra le composant défint dans chaque route.
- ``` <router-link></router-link> ```, similaire à la balise ``` anchor ``` en HTML, permet la navigation de l'utilisateur et évite de recharger systématiquement la page.

## Routage dynamique
- **Matching de route dynamique**
la variable dynamique modifiable dans l'URL est marquée par deux-points ```  :name```
```javaScript
const router = new Router({
  routes: [
    {path: '/item/:name', component: ItemDetail}
  ]
})
```